(Done)Changed wp-includes to be loaded from CDN
(Done)Add Last Modified time to PHP output
How to purge cache
Expire only if modified
    at edge node
    at upstream node
Fastcgi cache?
Fastcgi_stale on which php errors
FIREWALLS
Tinc VPN
SFTP/SSH users
GIT
Staging environments
Payments
Frontend
Ticket Support


SSL
Multisite
www to non-www

InfluxDB
Telegraf
Host Checks (Back to Sensu & Alerta)
Log Shipping
Log Rotate
SYSCTL Tweaking with NGINX backlog
Monitoring TCP performance?

Nginx:
    -(Done) Set max body size
    -(Done) Setting PHP-FPM status and Ping
    - (Done) Nginx Status
    - (Done)Nginx error page 404 should be passed to wordpress
    - Change 503 and etc as well

PHP:
    - Reduce revalidation time of OPCACHE
    - Request Log

Wordpress:
    - Default Permalink structure
    -(Done) How to handle WP_CONTENT_URL

Geo Distribution:
    - VPN setup
    - Latency Check
    